var config = require('../config.js')();
var { format } = require('url');
var APIFeeSubscriptionContract = require('@apifee/contracts/build/contracts/APIFeeSubscription.json');
var ethereumNodeURI = format(config.services.ethereumNode);
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(ethereumNodeURI));
console.log(`Ethereum node running on ${ethereumNodeURI}`);

function contractService() {
  return {
    getContract,
    contractExists,
    getCharges,
    verifySignature
  };

  function getContract(contractAddress) {
    try {
      return new web3.eth.Contract(APIFeeSubscriptionContract.abi, contractAddress);
    } catch (err) {
      console.log('error while getting contract', err);
      return;
    }
  }

  async function contractExists(web3Contract) {
    if (!web3Contract) {
      return false;
    }

    return await web3Contract.methods.getBalance().call() > 0;
  }

  async function getCharges(web3Contract) {
    const charges = [];
    const chargesLength = await web3Contract.methods.getChargesLength().call();

    let accumCredits = 0;
    for (let i = 0; i < chargesLength; i++) {
      const rawCharge = await web3Contract.methods.getCharge(i).call();

      const charge = {
        accumCredits: accumCredits,
        id: rawCharge[0],
        termsHash: rawCharge[1],
        credits: parseInt(rawCharge[2]),
        consumedCredits: parseInt(rawCharge[3]),
        valueInWei: rawCharge[4]
      };

      charges.push(charge);
      accumCredits += charge.credits;
    }

    return charges;
  }

  async function verifySignature(web3Contract, signature, credits) {
    try {
      const signatureParts = splitSignature(signature);
      await web3Contract.methods.verifySignature(credits, signatureParts.v, signatureParts.r, signatureParts.s).call();
      return true;
    } catch (err) {
      console.log('err', err)
      // Invalid signature
      return false;
    }
  }

  function splitSignature(signature) {
    const v = web3.utils.hexToNumber('0x' + signature.slice(130, 132));

    return {
      r: signature.slice(0, 66),
      s: '0x' + signature.slice(66, 130),
      v:  v >= 27 ? v : v + 27
    };
  }
};

module.exports = contractService();
