function creditService() {
  const usedCredits = {};
  const signatureByContractAddress = {};

  return {
    getCurrentState,
    hasUsedGreaterCredit,
    hasUsedCredit,
    getUsedCredits,
    useCredit,
    useCredits,
    unuseCredits,
    unuseCredit,
    saveSignature,
    getSignature
  };

  function getCurrentState(contractAddress) {
    return getUsedCreditsInternal(contractAddress);
  }

  function hasUsedGreaterCredit(credit, contractAddress) {
    return !!getUsedCreditsInternal(contractAddress).find(($credit) => $credit > credit);
  }

  function hasUsedCredit(credit, contractAddress) {
    return getUsedCreditsInternal(contractAddress).includes(credit);
  }

  function getUsedCredits(credits, contractAddress) {
    return credits.filter((credit) => hasUsedCredit(credit, contractAddress));
  }

  function useCredits(credits, contractAddress) {
    usedCredits[contractAddress] = getUsedCreditsInternal(contractAddress).concat(credits);
  }

  function useCredit(credit, contractAddress) {
    getUsedCreditsInternal(contractAddress).push(credit);
  }

  function unuseCredits(credits, contractAddress) {
    credits.forEach((credit) => unuseCredit(credit, contractAddress));
  }

  function unuseCredit(credit, contractAddress) {
    const usedCredits = getUsedCreditsInternal(contractAddress);

    const indexToBeRemoved = usedCredits.indexOf(credit);

    if (indexToBeRemoved !== -1) {
      usedCredits.splice(indexToBeRemoved);
    }
  }

  function saveSignature(credit, signature, contractAddress) {
    const currentSignature = getSignature(contractAddress);

    const currentCredit = currentSignature.credit || 0;

    if (credit > currentCredit) {
      signatureByContractAddress[contractAddress] = {
        credit: credit,
        signature: signature
      };
    }
  }

  function getSignature(contractAddress) {
    if (!signatureByContractAddress[contractAddress]) {
      signatureByContractAddress[contractAddress] = {};
    }

    return signatureByContractAddress[contractAddress];
  }

  function getUsedCreditsInternal(contractAddress) {
    if (!usedCredits[contractAddress]) {
      usedCredits[contractAddress] = [];
    }

    return usedCredits[contractAddress];
  }
};

module.exports = creditService();
