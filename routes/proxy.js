var createError = require('http-errors');
var config = require('../config.js')();
var express = require('express');
var router = express.Router();
var httpProxy = require('http-proxy');
var { format } = require('url');
var _ = require('lodash');
var creditService = require('../service/creditsService.js');
var contractService = require('../service/contractService.js');
var APIFeeSubscriptionContract = require('@apifee/contracts/build/contracts/APIFeeSubscription.json');

var implementationBaseURI = format(config.services.implementationURI);
var proxy = httpProxy.createProxyServer({});
console.log(`Forwarding traffic from /api/* to ${implementationBaseURI}`);

proxy.on('proxyRes', function (proxyRes, req, res) {

});

proxy.on('error', function (proxyRes, req, res) {
  // TODO Return credits to the pool
});

router.get('/*', async function(req, res, next) {
  console.log(`Forwarding to ${implementationBaseURI}${req.url}`);
  const shouldProceedProxyRequest = await processRequest(req, res);

  if (shouldProceedProxyRequest) {
    console.log(`Forwarding to ${implementationBaseURI}${req.url}`);

    proxy.web(req, res, {
      changeOrigin: true,
      target: implementationBaseURI
    });
  }
});

async function processRequest(req, res) {
  try {
    console.log(`Checking request preconditions`);
    await processRequestInternal(req);
    return true;
  } catch (err) {
    if (!err.status) {
      console.log('Error', err);

      err = {
        status: 500,
        message: err.message
      }
    }

    console.log('Error', err);
    res.writeHead(err.status, {"Content-Type": "application/json"});

    const invalidContractResponse = JSON.stringify({
      status: err.status,
      message: err.message
    });

    res.end(invalidContractResponse);
    return false;
  }
}

async function processRequestInternal(req) {
  const rawCredits = req.get('X-APIFee-Credits');
  const contractAddress = req.get('X-APIFee-Contract-Address');
  const signature = req.get('X-APIFee-Signature');

  console.log('X-APIFee-Credits', rawCredits);
  console.log('X-APIFee-Contract-Address', contractAddress);
  console.log('X-APIFee-Signature', signature);

  const credits = rawCredits
    .split(',')
    .map((rawCredit) => parseInt(rawCredit))
    .filter((rawCredit) => !_.isNaN(rawCredit) && _.isNumber(rawCredit));
  const maxCredit = _.max(credits);

  const web3Contract = await validateContract(contractAddress);
  const charges = await getAndValidateContractCharges(web3Contract);
  await validateSignature(web3Contract, signature, maxCredit);
  await validateCreditsHaveNotBeenUsedAndMarkAsUsed(credits, contractAddress, charges);
  saveSignature(maxCredit, signature, contractAddress);

  console.log('current signature', creditService.getSignature(contractAddress));
}

// Validations

async function validateContract(contractAddress) {
  const contract = await contractService.getContract(contractAddress);
  const contractExists = await contractService.contractExists(contract);

  // TODO Implement contract bytecode validation

  if (!contractExists) {
    throw createError(400, 'Invalid contract');
  }

  return contract;
}

async function getAndValidateContractCharges(web3Contract) {
  const charges = await contractService.getCharges(web3Contract);

  // TODO Validate charges agains apiFee

  return charges;
}

async function validateSignature(web3Contract, signature, credits) {
  const validSignature = await contractService.verifySignature(web3Contract, signature, credits);

  if (!validSignature) {
    throw createError(400, 'Invalid signature');
  }

  return validSignature;
}

async function validateCreditsHaveNotBeenUsedAndMarkAsUsed(credits, contractAddress, charges) {
  const usedCredits = creditService.getUsedCredits(credits, contractAddress);

  if (usedCredits.length > 0) {
    throw createError(402, `Credits ${usedCredits} have already been used`);
  }

  const maxCredit = _.max(credits);

  // If there is at least one credit greater thatn maxCredit means that is valid
  // There is an scenario where the provider could have withdrawn but the consumer
  // has not used some of the previous credits
  if (creditService.hasUsedGreaterCredit(maxCredit, contractAddress)) {
    // Mark credits as used
    creditService.useCredits(credits, contractAddress);
    return;
  }

  // Get charge that corresponds to maxCredit
  const charge = findCorrespondingCharge(charges, maxCredit);

  // Max credit is out of range || Current charge has not credits left
  if (!charge || (charge.accumCredits + charge.consumedCredits >= maxCredit)) {
    throw createError(402, `Run out of credits`);
  }

  // Mark credits as used
  creditService.useCredits(credits, contractAddress);
}

function saveSignature(credit, signature, contractAddress) {
  creditService.saveSignature(credit, signature, contractAddress);
}

function findCorrespondingCharge(charges, credit) {
  return charges.find((charge) => (charge.accumCredits + charge.credits) >= credit);
}

module.exports = router;
