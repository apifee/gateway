var express = require('express');
var router = express.Router();
var creditsService = require('../service/creditsService.js');
var contractService = require('../service/contractService.js');

/* GET home page. */
router.get('/:contractAddress', async function(req, res, next) {
  const contractAddress = req.params.contractAddress;
  const signature = creditsService.getSignature(contractAddress);

  // TODO try catch
  const web3Contract = await contractService.getContract(contractAddress);

  // Check valid contract
  if (!web3Contract) {
    res.writeHead(400, {"Content-Type": "application/json"});

    const jsonresponse = JSON.stringify({
      status: 400,
      message: 'Invalid contract address'
    });

    res.end(jsonresponse);
    return;
  }

  const charges = await contractService.getCharges(web3Contract);
  const usedCredits = creditsService.getCurrentState(contractAddress);

  let credits = 0;
  let consumedCredits = 0;
  let maxConsumed = 0;

  charges.forEach((charge) => {
    credits += charge.credits;
    consumedCredits += charge.consumedCredits;

    if (charge.consumedCredits > 0 && charge.consumedCredits < charge.credits) {
      maxConsumed = charge.accumCredits + charge.consumedCredits;
    }
  });

  // Credits that have not been withdrawn
  consumedCredits += usedCredits.filter((usedCredit) => usedCredit > maxConsumed).length;

  // Holes
  let holes = 0;
  for (let i = 1; i <= maxConsumed; i++) {
    if (!usedCredits.includes(i)) {
      holes++;
    }
  }

  consumedCredits -= holes;

  let creditsPendingWithdraw = 0;
  if (signature.credit) {
    creditsPendingWithdraw = signature.credit - maxConsumed;
  }

  res.writeHead(200, {"Content-Type": "application/json"});

  const jsonresponse = JSON.stringify({
    contractAddress: contractAddress,
    signature: signature,
    credits: credits,
    consumedCredits: consumedCredits,
    creditsPendingWithdraw,
    usedCredits: usedCredits
  });

  res.end(jsonresponse);
});

module.exports = router;
