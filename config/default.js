const _ = require('lodash');

module.exports = {
  express: {
    host: '0.0.0.0',
    port: _.parseInt(_.get(process.env, 'PORT', 3001))
  },
  services: {
    implementationURI: {
       protocol: _.get(process.env, 'IMPLEMENTATION_PROTOCOL', 'http'),
       hostname: _.get(process.env, 'IMPLEMENTATION_HOST', 'artii.herokuapp.com'),
       port:     _.parseInt(_.get(process.env, 'IMPLEMENTATION_PORT', 80)),
       pathname: _.get(process.env, 'IMPLEMENTATION_PATH', '/')
     },
     ethereumNode: {
       protocol: _.get(process.env, 'ETHEREUM_PROTOCOL', 'http'),
       hostname: _.get(process.env, 'ETHEREUM_HOST', '127.0.0.1'),
       port:     _.parseInt(_.get(process.env, 'IMPLEMENTATION_PORT', 8545))
     }
  }
};
